#Script to test the functionality of the server app
#Version 2
#Written By: Wesley Stevens
#23 November 2020

import requests
import json
import difflib
import sys

def check_for_duplicate_records(data):
    for i in range(1, len(data['posts'])):
        #Identical records would be sequential when sorting by id therefore check each record with the record that precedes it to check for duplicates
        if data['posts'][i]['id'] == data['posts'][i-1]['id']:
            return False
    return True

def check_for_ascending(data, sort_method):
    for i in range(1, len(data['posts'])):
        #Check that each record is higher or equal to the previous record
        if data['posts'][i][sort_method] < data['posts'][i-1][sort_method]:
            return False
    return True

def check_for_descending(data, sort_method):
    for i in range(1, len(data['posts'])):
        #Check that each record is higher or equal to the previous record
        if data['posts'][i][sort_method] > data['posts'][i-1][sort_method]:
            return False
    return True

#Test the /api/ping route
print('Testing /api/ping')
request_data = json.loads(requests.get("http://127.0.0.1:5000/api/ping").content)
with open ('proofs/ping_proof.json', 'r') as file:
    proof_data = json.loads(file.read())
assert request_data == proof_data, 'ERROR: PING TEST FAILED RESULT WAS:\n' + request_data + "\n\nEXPECTED RESULT WAS:\n" + proof_data
print('Ping test successful!')

#Test with one tag and check for duplicate records
print('Testing /api/posts?tags=tech')
response = requests.get("http://127.0.0.1:5000/api/posts?tags=tech")
if not response.ok:
    print("ERROR: Request to /api/posts?tags=tech failed, aborting test")
    sys.exit()
response_data = json.loads((response).content)
if not check_for_duplicate_records(response_data):
    print("ERROR: Duplicate records exist in response data")
    sys.exit()
print('Testing /api/posts?tags=tech successfully completed!')

#Test with multiple tags and check for duplicate records
print('Testing /api/posts?tags=tech,history,science')
response = requests.get("http://127.0.0.1:5000/api/posts?tags=tech,history,science")
if not response.ok:
    print("ERROR: Request to /api/posts?tags=tech,history,science failed, aborting test")
    sys.exit()
response_data = json.loads((response).content)
if not check_for_duplicate_records(response_data):
    print("ERROR: Duplicate records exist in response data")
    sys.exit()
print('Testing /api/posts?tags=tech,history,science successfully completed!')

#Test sorting by id
print('Testing /api/posts?tags=tech&sortBy=id')
response = requests.get("http://127.0.0.1:5000/api/posts?tags=tech&sortBy=id")
if not response.ok:
    print("ERROR: Request to /api/posts?tags=tech&sortBy=id failed, aborting test")
    sys.exit()
response_data = json.loads((response).content)
if not check_for_ascending(response_data, 'id'):
    print("ERROR: Records are not in order of id ascending")
    sys.exit()
print('Testing /api/posts?tags=tech&sortBy=id successfully completed!' )

#Test sorting by likes
print('Testing /api/posts?tags=tech&sortBy=likes')
response = requests.get("http://127.0.0.1:5000/api/posts?tags=tech&sortBy=likes")
if not response.ok:
    print("ERROR: Request to /api/posts?tags=tech&sortBy=likes failed, aborting test")
    sys.exit()
response_data = json.loads((response).content)
if not check_for_ascending(response_data, 'likes'):
    print("ERROR: Records are not in order of likes ascending")
    sys.exit()
print('Testing /api/posts?tags=tech&sortBy=likes successfully completed!' )

#Test sorting by likes
print('Testing /api/posts?tags=tech&sortBy=reads')
response = requests.get("http://127.0.0.1:5000/api/posts?tags=tech&sortBy=reads")
if not response.ok:
    print("ERROR: Request to /api/posts?tags=tech&sortBy=reads failed, aborting test")
    sys.exit()
response_data = json.loads((response).content)
if not check_for_ascending(response_data, 'reads'):
    print("ERROR: Records are not in order of reads ascending")
print('Testing /api/posts?tags=tech&sortBy=reads successfully completed!' )

#Test sorting by popularity
print('Testing /api/posts?tags=tech&sortBy=popularity')
response = requests.get("http://127.0.0.1:5000/api/posts?tags=tech&sortBy=popularity")
if not response.ok:
    print("ERROR: Request to /api/posts?tags=tech&sortBy=popularity failed, aborting test")
    sys.exit()
response_data = json.loads((response).content)
if not check_for_ascending(response_data, 'popularity'):
    print("ERROR: Records are not in order of popularity ascending")
    sys.exit()
print('Testing /api/posts?tags=tech&sortBy=popularity successfully completed!' )

#Test sorting by id descending
print('Testing /api/posts?tags=tech&sortBy=id&direction=desc')
response = requests.get("http://127.0.0.1:5000/api/posts?tags=tech&sortBy=id&direction=desc")
if not response.ok:
    print("ERROR: Request to /api/posts?tags=tech&sortBy=id&direction=desc failed, aborting test")
    sys.exit()
response_data = json.loads((response).content)
if not check_for_descending(response_data, 'id'):
    print("ERROR: Records are not in order of id descending")
    sys.exit()
print('Testing /api/posts?tags=tech&sortBy=id&direction=desc successfully completed!' )

print('All tests successfully complete!')
