# Back-end skills project v2
# Written by Wesley Stevens
# 20 November 2020

from flask import jsonify, make_response, request
import requests
import requests_cache
import json
import threading

from app import app

#Initialize cache thats saves requests for 3 minutes
requests_cache.install_cache('github_cache', backend='sqlite', expire_after=180)

#Request post data from Hatchways API
def send_request(request_string, list_of_dicts):
    response = requests.get(request_string)
    data = json.loads(response.content)
    print(response.from_cache)
    #Save to be combined later
    list_of_dicts.append(data)
    return

#Check connection to this api
@app.route('/api/ping')
def ping_api():
    data = {'success':True}
    return make_response(jsonify(data), 200)

#Retrieve posts with relevant tags
@app.route('/api/posts')
def retrieve_posts():
    tags = request.args.get('tags')
    sort_method = request.args.get('sortBy')
    direction = request.args.get('direction')

    #Return an error response if no tag is input
    if tags is None or tags == '':
        data = {'ERROR':'Tags parameter is missing!'}
        return make_response(jsonify(data), 400)

    #Return an error response if sortBy tag is invalid
    if sort_method is not None and sort_method not in ['id', 'likes', 'reads', 'popularity']:
        data = {'ERROR':'SortBy parameter is invalid'}
        return make_response(jsonify(data), 400)

    #Parse tags (if more than one)
    tag_array = tags.split(',')
    list_of_dicts = []
    complete_data = dict()
    threads = []

    for tag in tag_array:
        
        #Skips over an empty string as a tag input
        if tag == '':
            continue

        #Make requests
        request_string = 'https://api.hatchways.io/assessment/blog/posts?tag={0}'.format(tag)
    
        #Create thread to send request and process the results
        thread = threading.Thread(target=send_request, args=(request_string, list_of_dicts))
        thread.start()
        threads.append(thread)


    #Join the request threads
    for thread in threads:
        thread.join()

    complete_data = list_of_dicts[0]

    if len(list_of_dicts) > 1:
        for i in range(1, len(list_of_dicts)):
            for value in list_of_dicts[i]['posts']:
                if value not in complete_data['posts']:        
                    complete_data['posts'].append(value)

    #Verify defaults are used if method or direction aren't specified
    if sort_method is None:
        sort_method = 'id'

    if direction is None:
        direction = 'asc'

    #Sort the list by appropriate method
    if sort_method == 'id':
        complete_data['posts'] = sorted(complete_data['posts'], key = lambda e : e["id"])
    elif sort_method == 'reads':
        complete_data['posts'] = sorted(complete_data['posts'], key = lambda e : e["reads"])
    elif sort_method == 'likes':
        complete_data['posts'] = sorted(complete_data['posts'], key = lambda e : e["likes"])
    elif sort_method == 'popularity':
        complete_data['posts'] = sorted(complete_data['posts'], key = lambda e : e["popularity"])

    #If direction is descending then reverse the list
    if direction == "desc":
        complete_data["posts"].reverse()

    return json.dumps(complete_data)